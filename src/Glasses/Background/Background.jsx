import React from 'react'
import './style.scss'
import Models from '../Models/Models'
import ListGlasses from '../ListGlasses/ListGlasses'
import listGlasses from './data.json'
import { useState } from 'react'

const Background = () => {
  const [glasses, setGlasses] = useState(listGlasses[6]);
  const handleGlasses = (item) => { 
    setGlasses(item);
  } 
  return (
    <div className='Background h-screen'>
        <div className="overlay">
          <div className="top w-screen text-center py-10">
              <h1 className='text-2xl font-semibold'>TRY GLASSES APP ONLINE</h1>
          </div>
          <Models glasses={glasses}></Models>
          <ListGlasses data = {listGlasses} handleGlasses = {handleGlasses}></ListGlasses>
        </div>
    </div>
  )
}

export default Background