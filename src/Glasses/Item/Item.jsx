import React from 'react'

const Item = ({data, handleGlasses}) => {
  return (
    <div>
        <img onClick={() => { handleGlasses(data) }} className='w-4/5 border-2 p-2 ml-4' src={data.url} alt="..." />
    </div>
  )
}

export default Item