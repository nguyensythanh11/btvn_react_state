import React from 'react'
import './style.scss'
import Item from '../Item/Item'

const ListGlasses = (props) => {
  const {data, handleGlasses} = props
  return (
    <div className='ListGlasses container grid grid-cols-6'>
        {
            data.map((item) => { 
                return (
                    <Item data = {item} handleGlasses = {handleGlasses} key={item.id}></Item>
                )
            })
        }
    </div>
  )
}

export default ListGlasses