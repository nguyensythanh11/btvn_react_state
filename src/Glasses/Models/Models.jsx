import React from 'react'
import './style.scss'

const Models = ({glasses}) => {
  return (
    <div className='Models container grid grid-cols-2'>
        <div className="left flex justify-center items-center relative mt-8">
            <img className='w-2/5' src="./images/model.jpg" alt="..." />
            <div className="content absolute bottom-0 left-30 w-2/5">
                <h2>{glasses.name}</h2>
                <p>{glasses.desc}</p>
            </div>
            <img className='w-1/5 absolute top-20 left-30' src={glasses.url} alt="..." />
        </div>
        <div className="right flex justify-center items-center mt-8">
            <img className='w-2/5' src="./images/model.jpg" alt="..." />
        </div>
    </div>
  )
}

export default Models